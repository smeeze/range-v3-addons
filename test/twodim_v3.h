#pragma once

// range v3
#include <range/v3/view/drop.hpp>
#include <range/v3/view/stride.hpp>
#include <range/v3/view/take.hpp>

namespace test {
class twodim_v3 {
  int _width;
  int _height;

public:
  twodim_v3(int width, int height) : _width(width), _height(height) {}
  auto row(int n) const {
    return ranges::views::drop(n * _width) | ranges::views::take(_width);
  }
  auto column(int n) const {
    return ranges::views::drop(n) | ranges::views::stride(_width) |
           ranges::views::take(_height);
  }
};
} // namespace test
