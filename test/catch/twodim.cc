// system
#include <vector>
// catch
#include <catch2/catch.hpp>
// range v3
#include <range/v3/view/iota.hpp>
#include <range/v3/view/zip.hpp>
// self
#include <smeeze/range/twodim.h>
// test
#include "../twodim_v3.h"
#include "../utils.h"
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
const int width(5);
const int height(4);
const std::vector<int> v1{0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
                          10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
std::vector<int> v_tp_width_height{0,  5,  10, 15, 1,  6,  11, 16, 2,  7,
                                   12, 17, 3,  8,  13, 18, 4,  9,  14, 19};
std::vector<int> v_tp_height_width{0, 4, 8,  12, 16, 1, 5, 9,  13, 17,
                                   2, 6, 10, 14, 18, 3, 7, 11, 15, 19};
//
//
//
template <typename T1, typename T2>
void equal_container(const T1 &t1, const T2 &t2) {
  REQUIRE(ranges::distance(t1) == ranges::distance(t2));
  for (const auto &[z0, z1] : ranges::zip_view(t1, t2)) {
    REQUIRE(z0 == z1);
  }
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("range-v3-addons-twodim") {

  SECTION("diagonal") {
    equal_container(v1 | smeeze::range::twodim(width, height).diagonal(),
                    std::vector<int>{0, 6, 12, 18});

    equal_container(smeeze::range::twodim(width, height).diagonal()(v1),
                    std::vector<int>{0, 6, 12, 18});
  }

  SECTION("transpose") {
    equal_container(v1 | smeeze::range::twodim(width, height).transpose(),
                    v_tp_width_height);
    equal_container(v1 | smeeze::range::twodim(height, width).transpose(),
                    v_tp_height_width);

    equal_container(smeeze::range::twodim(width, height).transpose()(v1),
                    v_tp_width_height);
    equal_container(smeeze::range::twodim(height, width).transpose()(v1),
                    v_tp_height_width);
  }
  SECTION("row") {
    equal_container(v1 | smeeze::range::twodim(width, height).row(1),
                    std::vector<int>{5, 6, 7, 8, 9});

    equal_container(smeeze::range::twodim(width, height).row(1)(v1),
                    std::vector<int>{5, 6, 7, 8, 9});
  }
  SECTION("row_v3") {
    equal_container(smeeze::range::twodim(width, height).row(1)(v1),
                    test::twodim_v3(width, height).row(1)(v1));
  }
  SECTION("column") {
    equal_container(v1 | smeeze::range::twodim(width, height).column(1),
                    std::vector<int>{1, 6, 11, 16});

    equal_container(smeeze::range::twodim(width, height).column(1)(v1),
                    std::vector<int>{1, 6, 11, 16});
  }
  SECTION("column_v3") {
    equal_container(smeeze::range::twodim(width, height).column(1)(v1),
                    test::twodim_v3(width, height).column(1)(v1));
  }
  SECTION("sub") {
    equal_container(v1 | smeeze::range::twodim(width, height).sub(1, 1, 2, 2),
                    std::vector<int>{6, 7, 11, 12});

    equal_container(smeeze::range::twodim(width, height).sub(1, 1, 2, 2)(v1),
                    std::vector<int>{6, 7, 11, 12});
  }
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("range-v3-addons-twodim-modify") {
  SECTION("transpose-width-height") {
    auto v(test::make_zero_vector<int>(width * height));
    test::apply_iota(v | smeeze::range::twodim(width, height).transpose());
    equal_container(v, v_tp_height_width);
  }
  SECTION("transpose-height-width") {
    auto v(test::make_zero_vector<int>(width * height));
    test::apply_iota(v | smeeze::range::twodim(height, width).transpose());
    equal_container(v, v_tp_width_height);
  }
  SECTION("row") {
    auto v(test::make_zero_vector<int>(width * height));
    test::apply_iota(v | smeeze::range::twodim(width, height).row(1));
    equal_container(v, std::vector<int>{0, 0, 0, 0, 0, 0, 1, 2, 3, 4,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
  }
  SECTION("column") {
    auto v(test::make_zero_vector<int>(width * height));
    test::apply_iota(v | smeeze::range::twodim(width, height).column(1));
    equal_container(v, std::vector<int>{0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                                        0, 2, 0, 0, 0, 0, 3, 0, 0, 0});
  }
}
