// system
#include <vector>
// fmt
#include <fmt/format.h>
// range v3
#include <range/v3/algorithm/copy.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/reverse.hpp>
#include <range/v3/view/slice.hpp>
#include <range/v3/view/take.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/view/zip.hpp>
// self
#include <smeeze/range/twodim.h>
// test
#include "dump.h"
#include "utils.h"
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
void run_test_diagonal(const int &width, const int &height) {
  fmt::print("--- {} --\n", __PRETTY_FUNCTION__);
  std::vector<int> vi(width * height);
  test::apply_zero(vi);
  test::apply_iota(vi | smeeze::range::twodim(width, height).diagonal());
  test::dump("vi | diagonal", width, vi);
}
//
//
//
void run_test_transpose(const int &width, const int &height) {
  fmt::print("--- {} --\n", __PRETTY_FUNCTION__);
  test::dump("iota | transpose", height,
             ranges::views::iota(0) |
                 ranges::views::take(width * height) |
                 smeeze::range::twodim(width, height).transpose());
  test::dump("iota | transpose | slice", height,
             ranges::views::iota(0) |
                 ranges::views::take(width * height) |
                 smeeze::range::twodim(width, height).transpose() |
                 ranges::views::slice(height, height * 2));
  test::dump("iota | reverse | transpose", height,
             ranges::views::iota(0) |
                 ranges::views::take(width * height) |
                 ranges::views::reverse |
                 smeeze::range::twodim(width, height).transpose());
  test::dump("iota | transpose | reverse", height,
             ranges::views::iota(0) |
                 ranges::views::take(width * height) |
                 smeeze::range::twodim(width, height).transpose() |
                 ranges::views::reverse);
  test::dump("vi | transpose | transform", height,
             ranges::views::iota(0) |
                 ranges::views::take(width * height) |
                 smeeze::range::twodim(width, height).transpose() |
                 ranges::views::transform(
                     [](const auto &i) { return fmt::format("'{}'", i); }));
  test::dump("iota | transpose | transpose", width,
             ranges::views::iota(0) |
                 ranges::views::take(width * height) |
                 smeeze::range::twodim(width, height).transpose() |
                 smeeze::range::twodim(height, width).transpose());
  std::vector<int> vi(width * height);
  for (const auto [z0, z1] : ranges::views::zip(
           ranges::views::iota(50),
           vi | smeeze::range::twodim(width, height).transpose())) {
    z1 = z0;
  }
  test::dump("vi", width, vi);
}
//
//
//
void run_test_row(const int &width, const int &height) {
  fmt::print("--- {} --\n", __PRETTY_FUNCTION__);

  for (int h = 0; h < height; ++h) {
    test::dump(fmt::format("iota | row({})", h), width,
               ranges::views::iota(0, width * height) |
                   smeeze::range::twodim(width, height).row(h));
  }
}
//
//
//
void run_test_column(const int &width, const int &height) {
  fmt::print("--- {} --\n", __PRETTY_FUNCTION__);

  for (int w = 0; w < width; ++w) {
    test::dump(fmt::format("iota | column({})", w), height,
               ranges::views::iota(0, width * height) |
                   smeeze::range::twodim(width, height).column(w));
  }
}
//
//
//
void run_test_sub(const int &width, const int &height, const int &x,
                  const int &y, const int &w, const int &h) {
  fmt::print("--- {} --\n", __PRETTY_FUNCTION__);

  test::dump("iota | sub", w,
             ranges::views::iota(0, width * height) |
                 smeeze::range::twodim(width, height).sub(x, y, w, h));
  test::dump("iota | sub | transpose", h,
             ranges::views::iota(0, width * height) |
                 smeeze::range::twodim(width, height).sub(x, y, w, h) |
                 smeeze::range::twodim(w, h).transpose());
  test::dump("iota | sub | transpose | transform", h,
             ranges::views::iota(0, width * height) |
                 smeeze::range::twodim(width, height).sub(x, y, w, h) |
                 smeeze::range::twodim(w, h).transpose() |
                 ranges::views::transform(
                     [](const auto &i) { return fmt::format("'{}'", i); }));
}
//
//
//
void run_test_sub_set(const int &width, const int &height, const int &x,
                      const int &y, const int &w, const int &h) {
  fmt::print("--- {} --\n", __PRETTY_FUNCTION__);
  std::vector<int> vi(width * height);
#if 0
  ranges::copy(ranges::views::iota(50),
               vi | smeeze::range::twodim(width, height).sub(x, y, w, h));
#else
  for (const auto [z0, z1] : ranges::views::zip(
           ranges::views::iota(50),
           vi | smeeze::range::twodim(width, height).sub(x, y, w, h))) {
    z1 = z0;
  }
#endif
  test::dump("vi", width, vi);
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  run_test_diagonal(5, 4);
  run_test_transpose(5, 4);
  run_test_row(5, 4);
  run_test_column(5, 4);
  run_test_sub(5, 4, 1, 1, 3, 2);
  run_test_sub(10, 10, 3, 4, 5, 3);
  run_test_sub_set(6, 6, 2, 2, 3, 3);
  return 0;
}
