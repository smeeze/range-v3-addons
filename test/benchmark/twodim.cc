// system
#include <vector>
// benchmark
#include <benchmark/benchmark.h>
// self
#include <smeeze/range/twodim.h>
// test
#include "../twodim_v3.h"
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
void bm_vector_int(benchmark::State &state) {
  int n(0);
  std::vector<int> v((size_t)state.range(0));
  while (state.KeepRunning()) {
    for (auto &&i : v) {
      benchmark::DoNotOptimize(i = n);
    }
    ++n;
  }
}
//
//
//
void bm_smeeze_row(benchmark::State &state) {
  int n(0);
  auto width(state.range(0));
  auto height(state.range(1));
  std::vector<int> v((size_t)(width * height));
  while (state.KeepRunning()) {
    for (auto &&i : v | smeeze::range::twodim((int)width, (int)height).row(3)) {
      benchmark::DoNotOptimize(i = n);
    }
    ++n;
  }
}
//
//
//
void bm_smeeze_column(benchmark::State &state) {
  int n(0);
  auto width(state.range(0));
  auto height(state.range(1));
  std::vector<int> v((size_t)(width * height));
  while (state.KeepRunning()) {
    for (auto &&i :
         v | smeeze::range::twodim((int)width, (int)height).column(3)) {
      benchmark::DoNotOptimize(i = n);
    }
    ++n;
  }
}
//
//
//
void bm_smeeze_transpose(benchmark::State &state) {
  int n(0);
  auto width(state.range(0));
  auto height(state.range(1));
  std::vector<int> v((size_t)(width * height));
  while (state.KeepRunning()) {
    for (auto &&i :
         v | smeeze::range::twodim((int)width, (int)height).transpose()) {
      benchmark::DoNotOptimize(i = n);
    }
    ++n;
  }
}
//
//
//
void bm_smeeze_row_v3(benchmark::State &state) {
  int n(0);
  auto width(state.range(0));
  auto height(state.range(1));
  std::vector<int> v((size_t)(width * height));
  while (state.KeepRunning()) {
    for (auto &&i : v | test::twodim_v3((int)width, (int)height).row(3)) {
      benchmark::DoNotOptimize(i = n);
    }
    ++n;
  }
}
//
//
//
void bm_smeeze_column_v3(benchmark::State &state) {
  int n(0);
  auto width(state.range(0));
  auto height(state.range(1));
  std::vector<int> v((size_t)(width * height));
  while (state.KeepRunning()) {
    for (auto &&i : v | test::twodim_v3((int)width, (int)height).column(3)) {
      benchmark::DoNotOptimize(i = n);
    }
    ++n;
  }
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
BENCHMARK(bm_vector_int)->Arg(20)->Arg(2000);
BENCHMARK(bm_smeeze_row)->Args({5, 4})->Args({50, 40})->Args({500, 400});
BENCHMARK(bm_smeeze_row_v3)->Args({5, 4})->Args({50, 40})->Args({500, 400});
BENCHMARK(bm_smeeze_column)->Args({5, 4})->Args({50, 40})->Args({500, 400});
BENCHMARK(bm_smeeze_column_v3)->Args({5, 4})->Args({50, 40})->Args({500, 400});
BENCHMARK(bm_smeeze_transpose)->Args({5, 4})->Args({50, 40})->Args({500, 400});
