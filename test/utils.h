#pragma once

// system
#include <vector>
// range v3
#include <range/v3/view/iota.hpp>
#include <range/v3/view/zip.hpp>

namespace test {
//
//
//
template <typename C> void apply_zero(C &&c) {
  for (auto &i : c) {
    i = 0;
  }
}
//
//
//
template <typename C> void apply_iota(C &&c) {
  for (const auto &[z0, z1] :
       ranges::zip_view(c, ranges::views::iota(0))) {
    z0 = z1;
  }
}
//
//
//
template <typename T> std::vector<T> make_zero_vector(int n) {
  std::vector<T> rv(n);
  apply_zero(rv);
  return rv;
}
//
//
//
template <typename T> std::vector<T> make_iota_vector(int n) {
  std::vector<T> rv(n);
  apply_iota(rv);
  return rv;
}
} // namespace test
