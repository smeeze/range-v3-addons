#pragma once

// system
#include <string>
// fmt
#include <fmt/format.h>

namespace test {
//
//
//
template <typename Rng> void dump(std::string msg, int w, Rng &&rng) {
  int n(0);
  for (const auto &i : rng) {
    // header
    if (n == 0) {
      fmt::print("{:40}[", msg);
      n = 0;
      for (auto &c : msg) {
        c = ' ';
      }
    }
    // value
    fmt::print(" {:3}", i);
    ++n;
    // footer
    if (n == w) {
      fmt::print(" ]\n");
      n = 0;
    }
  }
  if (n) {
    fmt::print(" ... ]\n");
  }
}
} // namespace test
