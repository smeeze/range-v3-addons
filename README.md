# range v3 addons
collection of addons for the range-v3 library

## twodim view
allows you to iterate in two dimensions.
for example:

in c++:
```C++
int width(5);
int height(4);
ranges::v3::view::iota(0) |
  ranges::v3::view::take(width * height) |
  smeeze::range::twodim(width, height).transpose();
```
will give a range with the elements:

`[ 0 5 10 15 1 6 11 16 2 7 12 17 3 8 13 18 4 9 14 19 ]`

This range matches the columns of the following matrix:

```
[  0  1  2  3  4 ]
[  5  6  7  8  9 ]
[ 10 11 12 13 14 ]
[ 15 16 17 18 19 ]
```
