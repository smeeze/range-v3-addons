#pragma once

// range v3
#include <range/v3/view/drop.hpp>
#include <range/v3/view/take.hpp>
// self
#include <smeeze/range/details/twodim/pattern_offset.h>
#include <smeeze/range/details/twodim/pattern_short_long.h>
#include <smeeze/range/details/twodim/view.h>

namespace smeeze {
namespace range {
class twodim {
  int _width;
  int _height;

public:
  twodim(int width, int height) : _width(width), _height(height) {}
  twodim(const twodim &) = delete;
  twodim(twodim &&) = delete;
  twodim &operator=(const twodim &) = delete;
  twodim &operator=(twodim &&) = delete;
  //
  //
  //
  auto diagonal() const {
    return ranges::make_pipeable([=](auto &&rng) {
      auto b(std::begin(rng));
      using I = decltype(b);
      return smeeze::range::details::twodim::view<
          I, smeeze::range::details::twodim::pattern_offset>(
          b, {0, _width + 1, std::min(_width, _height)});
    });
  }
  //
  //
  //
  auto transpose() const {
    return ranges::make_pipeable([=](auto &&rng) {
      auto b(std::begin(rng));
      using I = decltype(b);
      return smeeze::range::details::twodim::view<
          I, smeeze::range::details::twodim::pattern_short_long>(
          b, {0, _width, _height, 1 - _width * _height, _width * _height});
    });
  }
  //
  //
  //
  auto sub(int x, int y, int w, int h) const {
    return ranges::make_pipeable([=](auto &&rng) {
      auto b(std::begin(rng));
      using I = decltype(b);
      return smeeze::range::details::twodim::view<
          I, smeeze::range::details::twodim::pattern_short_long>(
          b, {y * _width + x, 1, w, _width - w, w * h});
    });
  }
  //
  //
  //
  auto row(int n) const {
    return ranges::make_pipeable([=](auto &&rng) {
      auto b(std::begin(rng));
      using I = decltype(b);
      return smeeze::range::details::twodim::view<
          I, smeeze::range::details::twodim::pattern_offset>(
          b, {_width * n, 1, _width});
    });
  }
  //
  //
  //
  auto column(int n) const {
    return ranges::make_pipeable([=](auto &&rng) {
      auto b(std::begin(rng));
      using I = decltype(b);
      return smeeze::range::details::twodim::view<
          I, smeeze::range::details::twodim::pattern_offset>(
          b, {n, _width, _height});
    });
  }
};
} // namespace range
} // namespace smeeze
