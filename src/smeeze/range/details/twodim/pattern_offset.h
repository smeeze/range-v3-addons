#pragma once

namespace smeeze {
namespace range {
namespace details {
namespace twodim {
class pattern_offset {
public:
  pattern_offset() = default;
  pattern_offset(int start_offset, int stride, int count)
      : _position(0), _start_offset(start_offset), _stride(stride),
        _count(count) {}

  int prev(std::ptrdiff_t n) {
    _position -= (int)n;
    return _stride * -(int)n;
  }
  int next(std::ptrdiff_t n) {
    _position += (int)n;
    return _stride * (int)n;
  }

  int get_start_offset() const { return _start_offset; }
  int get_position() const { return _position; }
  int get_count() const { return _count; }

private:
  int _position;
  int _start_offset;
  int _stride;
  int _count;
};
} // namespace twodim
} // namespace details
} // namespace range
} // namespace smeeze
