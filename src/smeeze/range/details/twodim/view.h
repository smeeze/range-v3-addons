#pragma once

// range v3
#include <range/v3/all.hpp>

namespace smeeze {
namespace range {
namespace details {
namespace twodim {
//
//
//
template <typename I, typename Pattern>
class view : public ranges::view_facade<view<I, Pattern>> {
  struct cursor {
    I _iter;
    int _offset;
    Pattern _cursor_pattern;

    decltype(auto) read() { return *std::next(_iter, _offset); }
    decltype(auto) read() const { return *std::next(_iter, _offset); }

    void prev() { _offset += _cursor_pattern.prev(1); }
    void next() { _offset += _cursor_pattern.next(1); }

    void advance(std::ptrdiff_t n) {
      if (n > 0) {
        std::advance(_iter, _cursor_pattern.next(n));
      } else {
        std::advance(_iter, _cursor_pattern.prev(-n));
      }
    }

    std::ptrdiff_t distance_to(const cursor &other) const {
      return other._cursor_pattern.get_position() -
             _cursor_pattern.get_position();
    }

    bool equal(const cursor &other) const {
      return _cursor_pattern.get_position() ==
             other._cursor_pattern.get_position();
    }
  };

  friend ranges::range_access;

  cursor begin_cursor() const {
    return {_iter, _view_pattern.get_start_offset(), _view_pattern};
  }
  cursor end_cursor() const {
    Pattern tmp(_view_pattern);
    const int j(tmp.next(tmp.get_count()));
    return {_iter, tmp.get_start_offset() + j, tmp};
  }

  I _iter;
  Pattern _view_pattern;

public:
  view() = default;
  view(const I &i, Pattern &&pattern)
      : _iter(i), _view_pattern(std::forward<Pattern>(pattern)) {}
};
} // namespace twodim
} // namespace details
} // namespace range
} // namespace smeeze
