#pragma once

namespace smeeze {
namespace range {
namespace details {
namespace twodim {
class pattern_short_long {
public:
  pattern_short_long() = default;
  pattern_short_long(int start_offset, int short_stride, int short_stride_count,
                     int long_stride, int count)
      : _start_offset(start_offset), _position(0), _short_stride_test(0),
        _short_stride(short_stride), _short_count(short_stride_count),
        _long_stride(long_stride), _count(count) {}

  int prev(std::ptrdiff_t n) {
    _position -= (int)n;
    int rv(0);
    while (n--) {
      rv -= _short_stride;
      --_short_stride_test;
      if (_short_stride_test <= 0) {
        _short_stride_test = _short_count;
        rv -= _long_stride;
      }
    }
    return rv;
  }

  int next(std::ptrdiff_t n) {
    _position += (int)n;
    int rv(0);
    while (n--) {
      rv += _short_stride;
      ++_short_stride_test;
      if (_short_stride_test >= _short_count) {
        _short_stride_test = 0;
        rv += _long_stride;
      }
    }
    return rv;
  }

  int get_start_offset() const { return _start_offset; }
  int get_position() const { return _position; }
  int get_count() const { return _count; }

private:
  int _start_offset;
  int _position;
  int _short_stride_test;
  int _short_stride;
  int _short_count;
  int _long_stride;
  int _count;
};
} // namespace twodim
} // namespace details
} // namespace range
} // namespace smeeze
