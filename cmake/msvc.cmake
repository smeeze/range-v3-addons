if(WIN32)
  add_definitions(/W3)
  add_definitions(/WX)
# add_definitions(/wd4003)
# add_definitions(/wd4067)
# add_definitions(/wd4251)
# add_definitions(/wd4996)
  add_definitions(/permissive-)
# add_definitions(/experimental:preprocessor)
  add_definitions(-D__PRETTY_FUNCTION__=__FUNCSIG__)
endif() # WIN32
