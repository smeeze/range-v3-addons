if(NOT EXISTS ${BOOST_SOURCES}/boost/config.hpp)
  message(FATAL_ERROR "BOOST_SOURCES(${BOOST_SOURCES}) must point to valid boost source tree")
endif()

################
# boost system #
################
add_library(boost-system STATIC
  ${BOOST_SOURCES}/libs/system/src/error_code.cpp
)
target_include_directories(boost-system SYSTEM PRIVATE ${BOOST_SOURCES})
target_compile_definitions(boost-system        PRIVATE BOOST_ALL_NO_LIB)
# target_compile_definitions(boost-system        PRIVATE BOOST_ALL_DYN_LINK)
install(TARGETS boost-system DESTINATION lib)
