include(FetchContent)
FetchContent_Declare(
  benchmark
  URL https://github.com/google/benchmark/archive/v1.5.0.zip
)

FetchContent_GetProperties(benchmark)
if(NOT benchmark_POPULATED)
  MESSAGE("need to populate benchmark...")
  FetchContent_Populate(benchmark)
  set(BENCHMARK_ENABLE_GTEST_TESTS OFF CACHE BOOL "disable gtest")
  add_subdirectory(${benchmark_SOURCE_DIR} ${benchmark_BINARY_DIR} EXCLUDE_FROM_ALL)
  MESSAGE("done populating benchmark")
endif()
