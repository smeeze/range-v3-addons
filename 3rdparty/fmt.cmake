include(FetchContent)
FetchContent_Declare(
  fmt
  URL https://github.com/fmtlib/fmt/releases/download/6.0.0/fmt-6.0.0.zip
)

FetchContent_GetProperties(fmt)
if(NOT fmt_POPULATED)
  MESSAGE("need to populate fmt...")
  FetchContent_Populate(fmt)
  add_subdirectory(${fmt_SOURCE_DIR} ${fmt_BINARY_DIR} EXCLUDE_FROM_ALL)
  MESSAGE("done populating fmt")
endif()
