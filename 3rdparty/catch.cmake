include(FetchContent)
FetchContent_Declare(
  catch
  URL https://github.com/catchorg/Catch2/archive/v2.7.2.zip
)

FetchContent_GetProperties(catch)
if(NOT catch_POPULATED)
  MESSAGE("need to populate catch...")
  FetchContent_Populate(catch)
  set(CATCH_INCLUDE_DIR ${catch_SOURCE_DIR}/single_include PARENT_SCOPE)
  MESSAGE("done populating catch")
endif()
