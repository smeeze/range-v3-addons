include(FetchContent)
FetchContent_Declare(
  rangev3
  GIT_REPOSITORY https://github.com/ericniebler/range-v3.git
  GIT_TAG 0.9.1
)

FetchContent_GetProperties(rangev3)
if(NOT rangev3_POPULATED)
  MESSAGE("need to populate range-v3...")
  FetchContent_Populate(rangev3)
# set(RANGES_CXX_STD 17 CACHE STRING "ranges c++ standard")
# set(RANGES_PREFER_REAL_CONCEPTS OFF CACHE BOOL "no concepts")
  add_subdirectory(${rangev3_SOURCE_DIR} ${rangev3_BINARY_DIR})
  MESSAGE("done populating range-v3")
endif()
